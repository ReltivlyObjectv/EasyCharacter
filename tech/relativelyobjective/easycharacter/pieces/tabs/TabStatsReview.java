package tech.relativelyobjective.easycharacter.pieces.tabs;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import tech.relativelyobjective.easycharacter.utilities.InformationManager;
import tech.relativelyobjective.easycharacter.utilities.Lists;

/**
 *
 * @author ReltivlyObjectv
 */
public final class TabStatsReview extends JPanel {
	public TabStatsReview() {
		updateTabStatsReview();
	}
	public void updateTabStatsReview() {
		super.removeAll();
		super.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.ipady = 5;
		constraints.gridx = 0;
		constraints.gridy = 0;
		super.add(getHeaderLabel("Ability Scores"), constraints);
		constraints.gridy++;
		super.add(getStatsPanel(), constraints);
		constraints.gridy++;
		super.add(getHeaderLabel("Saving Throws"), constraints);
		constraints.gridy++;
		super.add(getSavingThrowsPanel(), constraints);
		constraints.gridy++;
		
		
	}
	private JLabel getHeaderLabel(String text) {
		JLabel returnMe = new JLabel(text);
		returnMe.setFont(new Font(returnMe.getFont().getFamily(), Font.BOLD, 20));
		return returnMe;
	}
	private JPanel getStatsPanel() {
		JPanel returnMe = new JPanel();
		returnMe.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.ipadx = 10;
		HashMap<Lists.Ability, Integer> scores = InformationManager.getAbilityScores();
		HashMap<Lists.Ability, Integer> maxValues = InformationManager.getMaxAbilityScores();
		for (int i = 0; i < 6; i++) {
			constraints.gridx = i;
			constraints.gridy = 0;
			Lists.Ability ab = Lists.Ability.values()[i];
			JLabel statLabel = new JLabel(String.format(
				"<html><strong>%s</strong></html>",
				InformationManager.capitalizeFirstLetterOfWords(ab)
			));
			returnMe.add(statLabel, constraints);
			int value = scores.get(ab);
			if (value > maxValues.get(ab)) {
				value = maxValues.get(ab);
			}
			int modifier = InformationManager.calculateStatModifier(value);
			JLabel statValue = new JLabel(String.format(
				"<html><div style='text-align: center;'>%d (%s%d)</div><html>",
				value,
				modifier < 0 ? "" : "+",
				modifier
			));
			constraints.gridy = 1;
			returnMe.add(statValue, constraints);
		}
		return returnMe;
	}
	private JPanel getSavingThrowsPanel() {
		JPanel returnMe = new JPanel();
		//TODO
		return returnMe;
	}
}
